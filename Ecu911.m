
function varargout = Ecu911(varargin)
% ECU911 MATLAB code for Ecu911.fig
%      ECU911, by itself, creates a new ECU911 or raises the existing
%      singleton*.
%
%      H = ECU911 returns the handle to a new ECU911 or the handle to
%      the existing singleton*.
%
%      ECU911('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ECU911.M with the given input arguments.
%
%      ECU911('Property','Value',...) creates a new ECU911 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Ecu911_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Ecu911_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools popmenu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Ecu911

% Last Modified by GUIDE v2.5 29-Oct-2014 19:04:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Ecu911_OpeningFcn, ...
                   'gui_OutputFcn',  @Ecu911_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Ecu911 is made visible.
function Ecu911_OpeningFcn(hObject, eventdata, handles, varargin)
global net;
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Ecu911 (see VARARGIN)
% Choose default command line output for Ecu911
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Ecu911 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Ecu911_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function btnEntrenar_Callback(hObject, eventdata, handles)

P= [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 ; 
    0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 ;
    0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1 0 0 0 0 1 1 1 1 ;
    0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 ;
    0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 ];

T=[0 0 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
   0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1;
   0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1;
   0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1];
global net
net = newff([0 1 ; 0 1 ; 0 1 ; 0 1; 0 1 ],[ 3 4 ], {'logsig' 'tansig' } , 'trainlm');
net.trainParam.show=5;
net.trainParam.epochs=1000;
net= train(net , P , T);
pesos = net.iw{1,1};
bias=net.b{1};

axes(handles.axes2);
plot(P);

axes(handles.axes3);
plot(T);


% --- Executes on button press in btnSimular.
function btnSimular_Callback(hObject, eventdata, handles)


% --- Executes on selection change in popmenu.
function popmenu_Callback(hObject, eventdata, handles)
% hObject    handle to popmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popmenu


% --- Executes during object creation, after setting all properties.
function popmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnLLamar.
function btnLLamar_Callback(hObject, eventdata, handles)
set(handles.bomberos,'String','');set(handles.fuerzasarmadas,'String','');set(handles.cruzroja,'String','');set(handles.policia,'String','');
val=[get(handles.incendio,'Value'),get(handles.accidente,'Value'),get(handles.inundaciones,'Value'),get(handles.deslaves,'Value'), get(handles.atentado,'Value')];
val;
global net;
s= sim(net,val);
resultado = s(1:4); % se obtiene la primera columna
%r = abs(resultado); %todos positivos
rf = int8(abs(resultado));%todos a entero
rf

axes(handles.axes3);
plot(rf);

cont=1;
for i=1:4
    
if (rf(i)==1)
    if (cont==1)
        %llamada='Bomberos'
        set(handles.bomberos,'String','Bomberos');
    elseif(cont==2)
        %llamada='Fuerzas Armadas'
        %llamada;
        set(handles.fuerzasarmadas,'String','Fuerzas Armadas');
    elseif (cont==3)
        set(handles.cruzroja,'String','Cruz Roja');
    elseif  (cont==4)
        set(handles.policia,'String','Policia');
    end
    cont=cont+1;
else
    cont=cont+1;
end
end 


% --- Executes on button press in incendio.
function incendio_Callback(hObject, eventdata, handles)
% hObject    handle to incendio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of incendio


% --- Executes on button press in accidente.
function accidente_Callback(hObject, eventdata, handles)
% hObject    handle to accidente (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of accidente


% --- Executes on button press in inundaciones.
function inundaciones_Callback(hObject, eventdata, handles)
% hObject    handle to inundaciones (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of inundaciones


% --- Executes on button press in atentado.
function atentado_Callback(hObject, eventdata, handles)
% hObject    handle to atentado (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of atentado


% --- Executes on button press in deslaves.
function deslaves_Callback(hObject, eventdata, handles)
% hObject    handle to deslaves (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of deslaves
